class DoctorsController  < ApplicationController
	before_action :set_doctor, only: [:show, :update, :destroy, :edit]
	def new
	  @doctor = Doctor.new
	end

	def create
	  @doctor = Doctor.new(doctor_params)

	  if @doctor.save
	    redirect_to doctors_path
	  else
	    render 'new'
	  end
	end

	def show
	end

	def index
	  @doctors = Doctor.all
	end

	def edit
	end

	def update

	  if @doctor.update(doctor_params)
	    redirect_to @doctor
	  else
	    render 'edit'
	  end
	end

	def destroy
	  @doctor.destroy

	  redirect_to doctors_path
	end

	private
	  def doctor_params
	    params.require(:doctor).permit(
	    	:fname,
	    	:lname,
	    	:email,
	    	:date_of_birth,
	    	:date_of_graduation
	    	)
	  end
	  def set_doctor
	  	 @doctor = Doctor.find(params[:id])
	  end
end
