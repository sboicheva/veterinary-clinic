class OwnersController  < ApplicationController
	before_action :set_owner, only: [:show, :update, :destroy, :edit]
	def new
	  @owner = Owner.new
	end

	def create
	  @owner = Owner.new(owner_params)

	  if @owner.save
	    redirect_to owners_path
	  else
	    render 'new'
	  end
	end

	def show
	end

	def index
	  @owners = Owner.all
	end

	def edit
	end

	def update

	  if @owner.update(owner_params)
	    redirect_to @owner
	  else
	    render 'edit'
	  end
	end

	def destroy
	  @owner.destroy

	  redirect_to owners_path
	end

	private
	  def owner_params
	    params.require(:owner).permit(
	    	:fname,
	    	:lname,
	    	:owner_of_an_animal
	    	)
	  end
	  def set_owner
	  	 @owner = Owner.find(params[:id])
	  end
end
