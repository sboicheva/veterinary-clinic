class Doctor < ActiveRecord::Base
	def shown_graduation_date
	  date_of_graduation.to_date
	end

	def shown_birth_date
	  date_of_birth.to_date 
	end
end
