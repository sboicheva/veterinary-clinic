class CreateDoctor < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
    	t.string :fname
    	t.string :lname
    	t.string :email
    	t.datetime :date_of_birth
    	t.datetime :date_of_graduation
    end
  end
end
