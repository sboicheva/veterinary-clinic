class CreateOwner < ActiveRecord::Migration
  def change
    create_table :owners do |t|
    	t.string :fname
    	t.string :lname
    	t.string :owner_of_an_animal
    end
  end
end
