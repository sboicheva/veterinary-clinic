class CreateAppointment < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
    	t.date :date
    	t.string :address
    end
  end
end
