class CreateAnimalPassport < ActiveRecord::Migration
  def change
    create_table :animal_passports do |t|
    	t.string :distinctive
    end
  end
end
